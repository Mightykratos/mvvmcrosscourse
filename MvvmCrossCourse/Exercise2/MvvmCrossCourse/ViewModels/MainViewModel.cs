using System.Threading.Tasks;
using MvvmCross.Core.ViewModels;
using System.Drawing;
using MvvmCross.Platform.UI;
using System;

namespace MvvmCrossCourse.Core.ViewModels
{
    public class MainViewModel : MvxViewModel
    {
        public string Title => "MainViewModel";

        public MainViewModel()
        {
        }

        public override Task Initialize()
        {
            return base.Initialize();
        }

        private int _blue = 0;
        public int Blue
        {
            get 
            { 
                return _blue; 
            }
            set 
            { 
                SetProperty(ref _blue, value); 
                RaisePropertyChanged(() => Color);
            }
        }

        private int _green = 0;
        public int Green
        {
            get 
            { 
                return _green; 
            }
            set 
            { 
                SetProperty(ref _green, value); 
                RaisePropertyChanged(() => Color);
            }
        }

        private int _red = 0;
        public int Red
        {
            get 
            {
                return _red; 
            }
            set 
            { 
                SetProperty(ref _red, value); 
                RaisePropertyChanged(() => Color);
            }
        }

        public MvxColor Color => new MvxColor(_red, _green, _blue);

        public IMvxCommand ResetColorCommand => new MvxCommand(Reset);

        private void Reset()
        {
            Red = 0;
            Green = 0;
            Blue = 0;
        }
    }
}