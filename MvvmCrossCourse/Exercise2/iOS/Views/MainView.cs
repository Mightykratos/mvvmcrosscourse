using MvvmCross.Binding.BindingContext;
using MvvmCross.iOS.Views;
using MvvmCross.iOS.Views.Presenters.Attributes;
using MvvmCrossCourse.Core.ViewModels;
using UIKit;
using MvvmCross.Plugins.Color;

namespace MvvmCrossCourse.iOS.Views
{
    [MvxRootPresentation(WrapInNavigationController = false)]
    public partial class MainView : MvxViewController<MainViewModel>
    {
        public MainView() : base("MainView", null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            Title = ViewModel.Title;

            BlueLayout.Layer.BorderColor = UIColor.Black.CGColor;
            BlueLayout.Layer.BorderWidth = 2f;
            RedLayout.Layer.BorderColor = UIColor.Black.CGColor;
            RedLayout.Layer.BorderWidth = 2f;
            GreenLayout.Layer.BorderColor = UIColor.Black.CGColor;
            GreenLayout.Layer.BorderWidth = 2f;

            var set = this.CreateBindingSet<MainView, MainViewModel>();
            set.Bind(Button).To(vm => vm.ResetColorCommand);
            set.Bind(LblText).For(x => x.TextColor).To(vm => vm.Color).WithConversion(new MvxNativeColorValueConverter());
            set.Bind(LblRed).To(Vm => Vm.Red);
            set.Bind(LblBlue).To(Vm => Vm.Blue);
            set.Bind(LblGreen).To(Vm => Vm.Green);
            set.Bind(RedSlider).For(x => x.Value).To(Vm => Vm.Red);
            set.Bind(BlueSlider).For(x => x.Value).To(Vm => Vm.Blue);
            set.Bind(GreenSlider).For(x => x.Value).To(Vm => Vm.Green);
            set.Apply();
        }
    }
}
