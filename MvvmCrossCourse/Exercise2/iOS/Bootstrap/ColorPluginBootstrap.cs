using MvvmCross.Platform.Plugins;

namespace MvvmCrossCourse.iOS.Bootstrap
{
    public class ColorPluginBootstrap
        : MvxLoaderPluginBootstrapAction<MvvmCross.Plugins.Color.PluginLoader, MvvmCross.Plugins.Color.iOS.Plugin>
    {
    }
}