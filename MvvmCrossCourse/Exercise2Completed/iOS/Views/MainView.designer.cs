// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace MvvmCrossCourse.iOS.Views
{
    [Register ("MainView")]
    partial class MainView
    {
        [Outlet]
        UIKit.UIView AlphaLayout { get; set; }


        [Outlet]
        UIKit.UISlider AlphaSlider { get; set; }


        [Outlet]
        UIKit.UIView BlueLayout { get; set; }


        [Outlet]
        UIKit.UISlider BlueSlider { get; set; }


        [Outlet]
        UIKit.UIView GreenLayout { get; set; }


        [Outlet]
        UIKit.UISlider GreenSlider { get; set; }


        [Outlet]
        UIKit.UILabel LblAlpha { get; set; }


        [Outlet]
        UIKit.UILabel LblBlue { get; set; }


        [Outlet]
        UIKit.UILabel LblGreen { get; set; }


        [Outlet]
        UIKit.UILabel LblRed { get; set; }


        [Outlet]
        UIKit.UILabel LblRotation { get; set; }


        [Outlet]
        UIKit.UILabel LblText { get; set; }


        [Outlet]
        UIKit.UIView RedLayout { get; set; }


        [Outlet]
        UIKit.UISlider RedSlider { get; set; }


        [Outlet]
        UIKit.UIStackView RGBALayout { get; set; }


        [Outlet]
        UIKit.UIView RotationLayout { get; set; }


        [Outlet]
        UIKit.UISlider RotationSlider { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton Button { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel TxtAlpha { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel TxtBlue { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel TxtGreen { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel TxtRed { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel TxtRotation { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (AlphaLayout != null) {
                AlphaLayout.Dispose ();
                AlphaLayout = null;
            }

            if (AlphaSlider != null) {
                AlphaSlider.Dispose ();
                AlphaSlider = null;
            }

            if (BlueLayout != null) {
                BlueLayout.Dispose ();
                BlueLayout = null;
            }

            if (BlueSlider != null) {
                BlueSlider.Dispose ();
                BlueSlider = null;
            }

            if (Button != null) {
                Button.Dispose ();
                Button = null;
            }

            if (GreenLayout != null) {
                GreenLayout.Dispose ();
                GreenLayout = null;
            }

            if (GreenSlider != null) {
                GreenSlider.Dispose ();
                GreenSlider = null;
            }

            if (LblAlpha != null) {
                LblAlpha.Dispose ();
                LblAlpha = null;
            }

            if (LblBlue != null) {
                LblBlue.Dispose ();
                LblBlue = null;
            }

            if (LblGreen != null) {
                LblGreen.Dispose ();
                LblGreen = null;
            }

            if (LblRed != null) {
                LblRed.Dispose ();
                LblRed = null;
            }

            if (LblRotation != null) {
                LblRotation.Dispose ();
                LblRotation = null;
            }

            if (LblText != null) {
                LblText.Dispose ();
                LblText = null;
            }

            if (RedLayout != null) {
                RedLayout.Dispose ();
                RedLayout = null;
            }

            if (RedSlider != null) {
                RedSlider.Dispose ();
                RedSlider = null;
            }

            if (RGBALayout != null) {
                RGBALayout.Dispose ();
                RGBALayout = null;
            }

            if (RotationLayout != null) {
                RotationLayout.Dispose ();
                RotationLayout = null;
            }

            if (RotationSlider != null) {
                RotationSlider.Dispose ();
                RotationSlider = null;
            }

            if (TxtAlpha != null) {
                TxtAlpha.Dispose ();
                TxtAlpha = null;
            }

            if (TxtBlue != null) {
                TxtBlue.Dispose ();
                TxtBlue = null;
            }

            if (TxtGreen != null) {
                TxtGreen.Dispose ();
                TxtGreen = null;
            }

            if (TxtRed != null) {
                TxtRed.Dispose ();
                TxtRed = null;
            }

            if (TxtRotation != null) {
                TxtRotation.Dispose ();
                TxtRotation = null;
            }
        }
    }
}