using MvvmCross.Binding.BindingContext;
using MvvmCross.iOS.Views;
using MvvmCross.iOS.Views.Presenters.Attributes;
using MvvmCrossCourse.Core.ViewModels;
using UIKit;
using MvvmCross.Plugins.Color;

namespace MvvmCrossCourse.iOS.Views
{
    [MvxRootPresentation(WrapInNavigationController = false)]
    public partial class MainView : MvxViewController<MainViewModel>
    {
        public MainView() : base("MainView", null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            Title = ViewModel.Title;

            BlueLayout.Layer.BorderColor = UIColor.Black.CGColor;
            BlueLayout.Layer.BorderWidth = 2f;
            RedLayout.Layer.BorderColor = UIColor.Black.CGColor;
            RedLayout.Layer.BorderWidth = 2f;
            GreenLayout.Layer.BorderColor = UIColor.Black.CGColor;
            GreenLayout.Layer.BorderWidth = 2f;
            AlphaLayout.Layer.BorderColor = UIColor.Black.CGColor;
            AlphaLayout.Layer.BorderWidth = 2f;
            RotationLayout.Layer.BorderColor = UIColor.Black.CGColor;
            RotationLayout.Layer.BorderWidth = 2f;

            var set = this.CreateBindingSet<MainView, MainViewModel>();
            set.Bind(Button).To(vm => vm.ResetColorCommand);
            set.Bind(LblText).For(x => x.TextColor).To(vm => vm.Color).WithConversion(new MvxNativeColorValueConverter());
            set.Bind(LblText).For(x => x.Transform).To(vm => vm.Rotation).WithConversion(new IntToTransformationValueConverter());
            set.Bind(LblRed).To(Vm => Vm.Red);
            set.Bind(LblBlue).To(Vm => Vm.Blue);
            set.Bind(LblGreen).To(Vm => Vm.Green);
            set.Bind(LblAlpha).To(Vm => Vm.Alpha);
            set.Bind(LblRotation).To(Vm => Vm.Rotation);
            set.Bind(RedSlider).For(x => x.Value).To(Vm => Vm.Red);
            set.Bind(BlueSlider).For(x => x.Value).To(Vm => Vm.Blue);
            set.Bind(GreenSlider).For(x => x.Value).To(Vm => Vm.Green);
            set.Bind(AlphaSlider).For(x => x.Value).To(Vm => Vm.Alpha);
            set.Bind(RotationSlider).For(x => x.Value).To(Vm => Vm.Rotation);
            set.Apply();
        }
    }
}
