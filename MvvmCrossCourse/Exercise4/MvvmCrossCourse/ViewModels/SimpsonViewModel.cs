using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using MvvmCross.Core.ViewModels;
using MvvmCrossCourse.Core.Data;

namespace MvvmCrossCourse.Core.ViewModels
{
    public class SimpsonViewModel : MvxViewModel
	{
		public string HeadshotUrl { get; set; }

        public string Title => Name ?? string.Empty;

		string _name;
		public string Name { 
            get { return _name; }
            set { SetProperty(ref _name, value);	}
		}

		string _email;
		public string Email {
            get { return _email; }
            set { SetProperty(ref _email, value); }
		}

        string _quote;
        public string Quote
        {
            get { return _quote; }
            set { SetProperty(ref _quote, value); }
        }

		DateTime _dob;
		public DateTime Dob {
            get { return _dob; }
            set { SetProperty(ref _dob, value);	}
		}

		Gender _gender;
		public Gender Gender {
            get { return _gender; }
            set { SetProperty(ref _gender, value); }
		}

        public override Task Initialize()
        {
            return base.Initialize();
        }

		public override string ToString()
		{
		    return this.Name;
		}
	}
    
}
