﻿using System;
using System.Collections.Generic;
using MvvmCross.Core.ViewModels;
using MvvmCross.Core.Navigation;
using System.Linq;

namespace MvvmCrossCourse.Core.ViewModels
{
    public class SimpsonsViewModel : MvxViewModel
    {
        private readonly IMvxNavigationService _mvxNavigationService;

        public string Title => "Simpsons";

        public IList<SimpsonViewModel> Simpsons { get; }

        public SimpsonsViewModel(IMvxNavigationService navigationService, ISimpsonsService simpsonsService)
        {
            _mvxNavigationService = navigationService;
            Simpsons = simpsonsService.GetSimpsons();
        }
    }
}
