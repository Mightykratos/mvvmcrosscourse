﻿using MvvmCross.Core.ViewModels;
using MvvmCrossCourse.Core.ViewModels;
namespace MvvmCrossCourse
{
    public class SimpsonDetailViewModel : MvxViewModel<SimpsonViewModel>
    {
        private SimpsonViewModel _simpsonViewModel { get; set; }
        public SimpsonViewModel SimpsonViewModel => _simpsonViewModel;

        public override void Prepare(SimpsonViewModel parameter)
        {
            this._simpsonViewModel = parameter;
        }
    }
}
