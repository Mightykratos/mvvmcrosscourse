﻿namespace MvvmCrossCourse.Core.Services
{
    public interface ITextToSpeechService
    {
        void Speak(string text);
    }
}
