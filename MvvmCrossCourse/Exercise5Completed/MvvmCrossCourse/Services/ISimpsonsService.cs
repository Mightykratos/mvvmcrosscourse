﻿using MvvmCrossCourse.Core.ViewModels;
using System.Collections.Generic;

namespace MvvmCrossCourse
{
    public interface ISimpsonsService
    {
        IList<SimpsonViewModel> GetSimpsons();
    }
}
