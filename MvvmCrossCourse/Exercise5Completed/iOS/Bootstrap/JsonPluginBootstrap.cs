using MvvmCross.Platform.Plugins;

namespace MvvmCrossCourse.iOS.Bootstrap
{
    public class JsonPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.Json.PluginLoader>
    {
    }
}