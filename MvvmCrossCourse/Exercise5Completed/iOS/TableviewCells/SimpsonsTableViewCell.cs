﻿using System;

using Foundation;
using UIKit;
using MvvmCross.Binding.iOS.Views;
using MvvmCross.Binding.BindingContext;
using MvvmCrossCourse.Core.ViewModels;
using MvvmCrossCourse.iOS.Converters;

namespace MvvmCrossCourse.iOS.TableViewCells
{
    public partial class SimpsonsTableViewCell : MvxTableViewCell
    {
        public static readonly NSString Key = new NSString("SimpsonsTableViewCell");
        public static readonly UINib Nib;

        static SimpsonsTableViewCell()
        {
            Nib = UINib.FromName("SimpsonsTableViewCell", NSBundle.MainBundle);
        }

        protected SimpsonsTableViewCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
            this.DelayBind(() =>
            {
                var set = this.CreateBindingSet<SimpsonsTableViewCell, SimpsonViewModel>();
                set.Bind(Name).To(vm => vm.Name);
                set.Bind(Email).To(vm => vm.Email);
                set.Bind(Image).To(vm => vm.HeadshotUrl).WithConversion(new ImageValueConverter());
                set.Apply();
            });
        }
    }
}
