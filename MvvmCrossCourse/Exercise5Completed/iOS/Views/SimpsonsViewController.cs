﻿using System;
using MvvmCross.iOS.Views;
using MvvmCross.iOS.Views.Presenters.Attributes;
using UIKit;
using MvvmCrossCourse.Core.ViewModels;
using MvvmCross.Binding.iOS.Views;
using MvvmCrossCourse.iOS.TableViewCells;
using MvvmCross.Binding.BindingContext;

namespace MvvmCrossCourse.iOS
{
    [MvxRootPresentation(WrapInNavigationController = true)]
    public partial class SimpsonsViewController : MvxViewController<SimpsonsViewModel>
    {
        public SimpsonsViewController() : base("SimpsonsViewController", null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            NavigationController.NavigationBar.Translucent = false;

            var source = new MvxSimpleTableViewSource(SimpsonsTableView, SimpsonsTableViewCell.Key);
            SimpsonsTableView.RowHeight = 70;

            var set = this.CreateBindingSet<SimpsonsViewController, SimpsonsViewModel>();
            set.Bind(source).To(vm => vm.Simpsons);
            set.Bind(source).For(x => x.SelectionChangedCommand).To(vm => vm.SimpsonSelectedCommand);
            set.Apply();

            SimpsonsTableView.Source = source;
            SimpsonsTableView.ReloadData();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            NavigationItem.Title = ViewModel.Title;
        }
    }
}

