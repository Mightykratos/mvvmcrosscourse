﻿using System;
using MvvmCross.Platform.Converters;
using UIKit;
using System.Reflection;
namespace MvvmCrossCourse.iOS.Converters
{
    public class ImageValueConverter : MvxValueConverter<string, UIImage>
    {
        protected override UIImage Convert(string value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return UIImage.FromBundle(value + ".gif");
        }
    }
}
