// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace MvvmCrossCourse.iOS
{
    [Register ("SimpsonsViewController")]
    partial class SimpsonsViewController
    {
        [Outlet]
        UIKit.UITableView TableView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView SimpsonsTableView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (SimpsonsTableView != null) {
                SimpsonsTableView.Dispose ();
                SimpsonsTableView = null;
            }
        }
    }
}