// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace MvvmCrossCourse.iOS.Views
{
    [Register ("SimpsonDetailViewController")]
    partial class SimpsonDetailViewController
    {
        [Outlet]
        UIKit.UILabel LblDob { get; set; }

        [Outlet]
        UIKit.UILabel LblEmail { get; set; }

        [Outlet]
        UIKit.UILabel LblGender { get; set; }

        [Outlet]
        UIKit.UILabel LblName { get; set; }

        [Outlet]
        UIKit.UILabel LblQuote { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView Image { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (Image != null) {
                Image.Dispose ();
                Image = null;
            }

            if (LblDob != null) {
                LblDob.Dispose ();
                LblDob = null;
            }

            if (LblEmail != null) {
                LblEmail.Dispose ();
                LblEmail = null;
            }

            if (LblGender != null) {
                LblGender.Dispose ();
                LblGender = null;
            }

            if (LblName != null) {
                LblName.Dispose ();
                LblName = null;
            }

            if (LblQuote != null) {
                LblQuote.Dispose ();
                LblQuote = null;
            }
        }
    }
}