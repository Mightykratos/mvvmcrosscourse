﻿using System;
using MvvmCross.Platform.Converters;
using System.Reflection;
using MvvmCrossCourse.Core.Data;

namespace MvvmCrossCourse.Droid.Converters
{
    public class DateTimeValueConverter : MvxValueConverter<DateTime, string>
    {
        protected override string Convert(DateTime value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value.ToString("d");
        }
    }
}
