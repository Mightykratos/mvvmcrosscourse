using MvvmCross.Platform.Plugins;

namespace MvvmCrossCourse.Droid.Bootstrap
{
    public class JsonPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.Json.PluginLoader>
    {
    }
}