﻿using System;
using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using MvvmCrossCourse.Core.ViewModels;

namespace MvvmCrossCourse.Core
{
    public class AppStart : MvxNavigatingObject, IMvxAppStart
    {
        private readonly IMvxNavigationService _mvxNavigationService;

        public AppStart(IMvxNavigationService mvxNavigationService)
        {
            _mvxNavigationService = mvxNavigationService;
        }

        public async void Start(object hint = null)
        {
            await _mvxNavigationService.Navigate<SimpsonsViewModel>();
        }
    }
}
