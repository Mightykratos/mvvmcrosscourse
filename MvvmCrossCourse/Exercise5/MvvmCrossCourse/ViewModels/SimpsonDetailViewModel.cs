﻿using System;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using MvvmCrossCourse.Core.ViewModels;
using MvvmCrossCourse.Core.Services;
namespace MvvmCrossCourse
{
    public class SimpsonDetailViewModel : MvxViewModel<SimpsonViewModel>
    {
        private SimpsonViewModel _simpsonViewModel { get; set; }
        public SimpsonViewModel SimpsonViewModel => _simpsonViewModel;

        public override void Prepare(SimpsonViewModel parameter)
        {
            this._simpsonViewModel = parameter;
        }
    }
}
