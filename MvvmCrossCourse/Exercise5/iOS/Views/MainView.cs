using MvvmCross.Binding.BindingContext;
using MvvmCross.iOS.Views;
using MvvmCross.iOS.Views.Presenters.Attributes;
using MvvmCrossCourse.Core.ViewModels;
using MvvmCross.iOS.Support.Views;

namespace MvvmCrossCourse.iOS.Views
{
    [MvxRootPresentation(WrapInNavigationController = false)]
    public partial class MainView : MvxBaseViewController<MainViewModel>
    {
        public MainView() : base()
        {
        }
    }
}
