﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace MvvmCrossCourse
{
    public interface IApiWebclientService
    {
        Task<T> GetAsync<T>(string url);
        Task<HttpResponseMessage> PostAsync(string url,object content);
    }
}
