﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using MvvmCrossCourse.Core.Api;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MvvmCrossCourse
{
    public class ApiWebclientService : IApiWebclientService
    {
        public HttpClient CreateClient()
        {
            var client = new HttpClient(new ModernHttpClient.NativeMessageHandler());
            client.BaseAddress = new Uri(Urls.BASE_URL);
            return client;
        }

        public async Task<T> GetAsync<T>(string url)
        {
            var client = CreateClient();
            if (client.DefaultRequestHeaders.CacheControl == null)
                client.DefaultRequestHeaders.CacheControl = new CacheControlHeaderValue();

            client.DefaultRequestHeaders.CacheControl.NoCache = true;
            client.DefaultRequestHeaders.IfModifiedSince = DateTime.UtcNow;
            client.DefaultRequestHeaders.CacheControl.NoStore = true;
            client.Timeout = new TimeSpan(0, 0, 30);
            var request = url;

            var response = await client.GetStringAsync(request);
            return await DeserializeObjectAsync<T>(response);
        }

        public async Task<HttpResponseMessage> PostAsync(string url, object item)
        {
            var client = CreateClient();
            if (client.DefaultRequestHeaders.CacheControl == null)
                client.DefaultRequestHeaders.CacheControl = new CacheControlHeaderValue();

            client.DefaultRequestHeaders.CacheControl.NoCache = true;
            client.DefaultRequestHeaders.IfModifiedSince = DateTime.UtcNow;
            client.DefaultRequestHeaders.CacheControl.NoStore = true;
            client.Timeout = new TimeSpan(0, 0, 30);
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;

            var json = JsonConvert.SerializeObject(item,settings);
            JObject rss = JObject.Parse(json);
            rss.Remove("Created");
            var content = new StringContent(rss.ToString());

            var request = url;

            return await client.PostAsync(request, content);
        }

        public Task<T> DeserializeObjectAsync<T>(string value)
        {
            return Task.Factory.StartNew(() => JsonConvert.DeserializeObject<T>(value));
        }

        public T DeserializeObject<T>(string value)
        {
            return JsonConvert.DeserializeObject<T>(value);
        }
    }
}
