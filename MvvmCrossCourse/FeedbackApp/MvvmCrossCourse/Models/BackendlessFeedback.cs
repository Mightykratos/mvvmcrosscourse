﻿using System;
using Newtonsoft.Json;

namespace MvvmCrossCourse
{
    public class BackendlessFeedback
    {
        public string ObjectId { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
        public string Feedback { get; set; }
        public long Created { get; set; }

        public bool Equals(BackendlessFeedback other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(ObjectId, other.ObjectId);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((BackendlessFeedback)obj);
        }

        public override int GetHashCode()
        {
            return (ObjectId != null ? ObjectId.GetHashCode() : 0);
        }
    }
}
