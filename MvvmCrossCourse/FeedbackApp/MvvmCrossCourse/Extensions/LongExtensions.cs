﻿using System;
namespace MvvmCrossCourse
{
    public static class LongExtensions
    {
        public static DateTime ToDateTime(this long input)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(input).ToLocalTime();
        }
    }
}
