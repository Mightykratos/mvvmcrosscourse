﻿using System;
using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using MvvmCrossCourse.Core.Api;
using MvvmCross.Platform;
using Acr.UserDialogs;

namespace MvvmCrossCourse
{
    public class AddFeedbackViewModel : MvxViewModel
    {
        private readonly IMvxNavigationService _mvxNavigationService;
        private readonly IApiWebclientService _webclient;

        public string Title => "Add Feedback";

        public AddFeedbackViewModel(IMvxNavigationService navigationService, IApiWebclientService webclient)
        {
            _mvxNavigationService = navigationService;
            _webclient = webclient;
        }

        string _firstname;
        public string FirstName
        {
            get { return _firstname; }
            set { SetProperty(ref _firstname, value); }
        }

        string _lastname;
        public string LastName
        {
            get { return _lastname; }
            set { SetProperty(ref _lastname, value); }
        }

        string _feedback;
        public string Feedback
        {
            get { return _feedback; }
            set { SetProperty(ref _feedback, value); }
        }

        public IMvxCommand AddFeedbackCommand => new MvxCommand(AddFeedback);

        private async void AddFeedback()
        {
            if (Feedback != string.Empty)
            {
                var response = await _webclient.PostAsync(Urls.FEEDBACK_URL, new BackendlessFeedback()
                {
                    Feedback = Feedback,
                    Name = FirstName,
                    Lastname = LastName
                });
                if(response.IsSuccessStatusCode)
                {
                    await Mvx.Resolve<IUserDialogs>().AlertAsync("Feedback is successvol verzonden", "Success","Ok");
                    await _mvxNavigationService.Close(this);
                }else
                {
                    await Mvx.Resolve<IUserDialogs>().AlertAsync("Het verzenden van de feedback is niet gelukt, Probeer het opnieuw", "Fout", "Ok");
                }
            }
        }

        public override void ViewAppearing()
        {
            base.ViewAppearing();
        }
    }
}
