﻿using System;
using System.Threading.Tasks;
using MvvmCross.Core.ViewModels;
namespace MvvmCrossCourse.Core.ViewModels
{
    public class FeedbackItemViewModel : MvxViewModel
    {
        public string Title => FullName ?? string.Empty;

        public BackendlessFeedback BackendlessFeedback { get; }

        public FeedbackItemViewModel(BackendlessFeedback feedback)
        {
            BackendlessFeedback = feedback;
        }

        public string Name => BackendlessFeedback.Name;

        public string LastName => BackendlessFeedback.Lastname;

        public string Created => BackendlessFeedback.Created.ToDateTime().ToString("d");

        public string Feedback => BackendlessFeedback.Feedback;

        public string FullName => BackendlessFeedback.Lastname == null ? BackendlessFeedback.Name : $"{BackendlessFeedback.Name} {BackendlessFeedback.Lastname}";

        public override string ToString()
        {
            return this.FullName;
        }
    }
}
