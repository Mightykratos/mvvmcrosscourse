﻿using System;
using MvvmCross.Core.ViewModels;
namespace MvvmCrossCourse.Core.ViewModels
{
    public class FeedbackDetailsViewModel : MvxViewModel<FeedbackItemViewModel>
    {
        private FeedbackItemViewModel _feedbackViewModel { get; set; }
        public FeedbackItemViewModel FeedbackViewModel => _feedbackViewModel;

        public override void Prepare(FeedbackItemViewModel parameter)
        {
            this._feedbackViewModel = parameter;
        }
    }
}
