﻿using MvvmCross.Core.ViewModels;
using MvvmCross.Core.Navigation;
using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using MvvmCrossCourse.Core.Api;

namespace MvvmCrossCourse.Core.ViewModels
{
    public class FeedbackViewModel : MvxViewModel
    {
        private readonly IMvxNavigationService _mvxNavigationService;
        private readonly IApiWebclientService _webclient;

        public string Title => "Feedback";

        private IList<BackendlessFeedback> _feedbackItems = new List<BackendlessFeedback>();

        private IList<FeedbackItemViewModel> _allFeedbackItems;
        public IList<FeedbackItemViewModel> AllFeedbackItems
        {
            get
            {
                _allFeedbackItems = new ObservableCollection<FeedbackItemViewModel>(_feedbackItems.Select(x => new FeedbackItemViewModel(x))).OrderBy(x => x.Created).ToList();
                return _allFeedbackItems;
            }
        }

        public FeedbackViewModel(IMvxNavigationService navigationService, IApiWebclientService webclient)
        {
            _mvxNavigationService = navigationService;
            _webclient = webclient;
        }

        public override void ViewAppearing()
        {
            base.ViewAppearing();
            GetFeedbackItems();
        }

        private async void GetFeedbackItems()
        {
            var feedbackList = await _webclient.GetAsync<IList<BackendlessFeedback>>(Urls.FEEDBACK_URL);
            _feedbackItems = feedbackList;
            _allFeedbackItems = null;
            RaisePropertyChanged(() => AllFeedbackItems);
        }

        public IMvxCommand FeedbackItemSelectedCommand => new MvxCommand<FeedbackItemViewModel>(ShowFeedback);

        private void ShowFeedback(FeedbackItemViewModel feedback)
        {
            _mvxNavigationService.Navigate<FeedbackDetailsViewModel, FeedbackItemViewModel>(feedback);
        }

        public IMvxCommand AddFeedbackCommand => new MvxCommand(AddFeedback);

        private void AddFeedback()
        {
            _mvxNavigationService.Navigate<AddFeedbackViewModel>();
        }
    }
}
