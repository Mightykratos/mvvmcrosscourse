﻿using Android.OS;
using Android.Runtime;
using Android.Views;
using MvvmCross.Binding.Droid.BindingContext;
using MvvmCross.Droid.Views.Attributes;
using MvvmCrossCourse.Core.ViewModels;
using MvvmCrossCourse.Droid.Views;

namespace MvvmCrossCourse.Droid
{
    [MvxFragmentPresentation(typeof(MainViewModel), Resource.Id.content_frame, true)]
    [Register(nameof(FeedbackDetailsFragment))]
    public class FeedbackDetailsFragment : MvvmCross.Droid.Views.Fragments.MvxFragment<FeedbackDetailsViewModel>
    {
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            var view = this.BindingInflate(Resource.Layout.fragment_feedback_details, null);
            if (Activity is MainActivity mainActivity)
            {
                if (mainActivity.SupportActionBar != null)
                {
                    mainActivity.SupportActionBar.Title = ViewModel.FeedbackViewModel.FullName;
                    mainActivity.SupportActionBar.SetDisplayHomeAsUpEnabled(true);
                }
            }
            return view;
        }

        public override void OnDetach()
        {
            base.OnDetach();
            if (Activity is MainActivity mainActivity)
            {
                if (mainActivity.SupportActionBar != null)
                {
                    mainActivity.SupportActionBar.SetDisplayHomeAsUpEnabled(false);
                }
            }
        }
    }
}
