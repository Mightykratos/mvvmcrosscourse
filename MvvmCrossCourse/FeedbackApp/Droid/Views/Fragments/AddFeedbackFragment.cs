﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using MvvmCross.Binding.Droid.BindingContext;
using MvvmCross.Droid.Views.Attributes;
using MvvmCrossCourse.Core.ViewModels;
using MvvmCrossCourse.Droid.Views;

namespace MvvmCrossCourse.Droid
{
    [MvxFragmentPresentation(typeof(MainViewModel), Resource.Id.content_frame, true)]
    [Register(nameof(AddFeedbackFragment))]
    public class AddFeedbackFragment : MvvmCross.Droid.Views.Fragments.MvxFragment<AddFeedbackViewModel>
    {
        
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            var view = this.BindingInflate(Resource.Layout.fragment_add_feedback, null);
            if (Activity is MainActivity mainActivity)
            {
                if (mainActivity.SupportActionBar != null)
                {
                    mainActivity.SupportActionBar.Title = ViewModel.Title;
                    mainActivity.SupportActionBar.SetDisplayHomeAsUpEnabled(true);
                }
            }
            return view;
        }

        public override void OnDetach()
        {
            base.OnDetach();
            if (Activity is MainActivity mainActivity)
            {
                if (mainActivity.SupportActionBar != null)
                {
                    mainActivity.SupportActionBar.SetDisplayHomeAsUpEnabled(false);
                }
            }
        }
    }
}
