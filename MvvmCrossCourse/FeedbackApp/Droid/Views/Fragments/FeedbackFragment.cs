﻿using Android.OS;
using Android.Runtime;
using Android.Views;
using MvvmCross.Binding.Droid.BindingContext;
using MvvmCross.Droid.Views.Attributes;
using MvvmCrossCourse.Core.ViewModels;
using MvvmCrossCourse.Droid.Views;

namespace MvvmCrossCourse.Droid
{
    [MvxFragmentPresentation(typeof(MainViewModel), Resource.Id.content_frame, false)]
    [Register(nameof(FeedbackFragment))]
    public class FeedbackFragment : MvvmCross.Droid.Views.Fragments.MvxFragment<FeedbackViewModel>
    {
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            var view = this.BindingInflate(Resource.Layout.fragment_feedback, null);
            if (Activity is MainActivity)
            {
                var mainActivity = ((MainActivity)Activity);

                if (mainActivity.SupportActionBar != null)
                {
                    mainActivity.SupportActionBar.Title = ViewModel.Title;
                }
            }
            return view;
        }
    }
}
