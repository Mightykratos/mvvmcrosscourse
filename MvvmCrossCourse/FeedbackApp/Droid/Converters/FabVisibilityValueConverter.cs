﻿using System;
using MvvmCross.Platform.Converters;
using Android.Opengl;
using Android.Transitions;
using Android.Views;
namespace MvvmCrossCourse.Droid
{
    public class FabVisibilityValueConverter : MvxValueConverter<string,ViewStates>
    {
        protected override ViewStates Convert(string value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (value == string.Empty) || (value == null) ? ViewStates.Gone : ViewStates.Visible;
        }
    }
}
