﻿using System;
using MvvmCross.Platform.Converters;
namespace MvvmCrossCourse.iOS
{
    public class TextToEnableButtonValueConverter : MvxValueConverter<string,bool>
    {
        protected override bool Convert(string value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (value == string.Empty)||(value == null) ? true : false;
        }
    }
}
