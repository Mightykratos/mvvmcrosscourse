﻿using System;
using MvvmCross.iOS.Views;
using UIKit;
using MvvmCrossCourse.Core.ViewModels;
using MvvmCross.iOS.Views.Presenters.Attributes;
using MvvmCross.Binding.iOS.Views;
using MvvmCross.Binding.BindingContext;
using MvvmCrossCourse.iOS.TableViewCells;

namespace MvvmCrossCourse.iOS
{
    [MvxRootPresentation(WrapInNavigationController = true)]
    public partial class FeedbackViewController : MvxViewController<FeedbackViewModel>
    {
        public FeedbackViewController() : base("FeedbackViewController", null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            NavigationController.NavigationBar.Translucent = false;
            NavigationController.NavigationBar.BarTintColor = UIColor.FromRGB(235, 236, 239);

            var addFeedbackBtn = new UIBarButtonItem(UIBarButtonSystemItem.Add)
            {
                Style = UIBarButtonItemStyle.Done
            };
            NavigationItem.RightBarButtonItem = addFeedbackBtn;

            var source = new MvxSimpleTableViewSource(FeedbackTableView, FeedbackTableViewCell.Key);
            FeedbackTableView.RowHeight = 50;

            var set = this.CreateBindingSet<FeedbackViewController, FeedbackViewModel>();
            set.Bind(source).To(vm => vm.AllFeedbackItems);
            set.Bind(source).For(x => x.SelectionChangedCommand).To(vm => vm.FeedbackItemSelectedCommand);
            set.Bind(addFeedbackBtn).To(vm => vm.AddFeedbackCommand);
            set.Apply();

            FeedbackTableView.Source = source;
            FeedbackTableView.ReloadData();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            NavigationItem.Title = ViewModel.Title;
        }
    }
}

