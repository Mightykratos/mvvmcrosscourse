// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace MvvmCrossCourse.iOS
{
	[Register ("FeedbackDetailsViewController")]
	partial class FeedbackDetailsViewController
	{
		[Outlet]
		UIKit.UILabel Date { get; set; }

		[Outlet]
		UIKit.UITextView Feedback { get; set; }

		[Outlet]
		UIKit.UILabel Name { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (Name != null) {
				Name.Dispose ();
				Name = null;
			}

			if (Feedback != null) {
				Feedback.Dispose ();
				Feedback = null;
			}

			if (Date != null) {
				Date.Dispose ();
				Date = null;
			}
		}
	}
}
