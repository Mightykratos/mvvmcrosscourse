﻿using System;
using MvvmCross.iOS.Views;
using MvvmCross.iOS.Views.Presenters.Attributes;
using UIKit;
using MvvmCross.Binding.BindingContext;

namespace MvvmCrossCourse.iOS
{
    [MvxChildPresentation()]
    public partial class AddFeedbackViewController : MvxViewController<AddFeedbackViewModel>
    {
        public AddFeedbackViewController() : base("AddFeedbackViewController", null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
            NavigationController.NavigationBar.Translucent = false;
            NavigationController.NavigationBar.BarTintColor = UIColor.FromRGB(235, 236, 239);

            Feedback.Layer.CornerRadius = 8;
            Feedback.Layer.BorderColor = UIColor.FromRGB(229,229,228).CGColor;
            Feedback.Layer.BorderWidth = 2;

            var set = this.CreateBindingSet<AddFeedbackViewController, AddFeedbackViewModel>();
            set.Bind(Name).To(vm => vm.FirstName);
            set.Bind(Lastname).To(vm => vm.LastName);
            set.Bind(Feedback).To(vm => vm.Feedback);
            set.Bind(SendFeedbackBtn).For(x => x.Hidden).To(vm => vm.Feedback).WithConversion(new TextToEnableButtonValueConverter());
            set.Bind(SendFeedbackBtn).To(vm => vm.AddFeedbackCommand);
            set.Apply();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            NavigationItem.Title = ViewModel.Title;
        }
    }
}

