// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace MvvmCrossCourse.iOS
{
	[Register ("AddFeedbackViewController")]
	partial class AddFeedbackViewController
	{
		[Outlet]
		UIKit.UITextView Feedback { get; set; }

		[Outlet]
		UIKit.UITextField Lastname { get; set; }

		[Outlet]
		UIKit.UITextField Name { get; set; }

		[Outlet]
		UIKit.UIButton SendFeedbackBtn { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (Feedback != null) {
				Feedback.Dispose ();
				Feedback = null;
			}

			if (Name != null) {
				Name.Dispose ();
				Name = null;
			}

			if (SendFeedbackBtn != null) {
				SendFeedbackBtn.Dispose ();
				SendFeedbackBtn = null;
			}

			if (Lastname != null) {
				Lastname.Dispose ();
				Lastname = null;
			}
		}
	}
}
