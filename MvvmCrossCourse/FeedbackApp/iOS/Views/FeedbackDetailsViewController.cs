﻿using System;
using MvvmCross.iOS.Views;
using MvvmCross.iOS.Views.Presenters.Attributes;
using UIKit;
using MvvmCrossCourse.Core.ViewModels;
using MvvmCross.Binding.BindingContext;

namespace MvvmCrossCourse.iOS
{
    [MvxChildPresentation()]
    public partial class FeedbackDetailsViewController : MvxViewController<FeedbackDetailsViewModel>
    {
        public FeedbackDetailsViewController() : base("FeedbackDetailsViewController", null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
            NavigationController.NavigationBar.Translucent = false;
            NavigationController.NavigationBar.BarTintColor = UIColor.FromRGB(235, 236, 239);

            var set = this.CreateBindingSet<FeedbackDetailsViewController, FeedbackDetailsViewModel>();
            set.Bind(Name).To(x => x.FeedbackViewModel.FullName);
            set.Bind(Feedback).To(x => x.FeedbackViewModel.Feedback);
            set.Bind(Date).To(x => x.FeedbackViewModel.Created);
            set.Apply();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            NavigationItem.Title = ViewModel.FeedbackViewModel.FullName;
        }
    }
}

