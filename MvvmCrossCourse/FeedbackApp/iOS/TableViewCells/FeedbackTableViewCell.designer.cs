// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace MvvmCrossCourse.iOS.TableViewCells
{
    [Register ("FeedbackTableViewCell")]
    partial class FeedbackTableViewCell
    {
        [Outlet]
        UIKit.UILabel Feedback { get; set; }


        [Outlet]
        UIKit.UILabel Name { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (Feedback != null) {
                Feedback.Dispose ();
                Feedback = null;
            }

            if (Name != null) {
                Name.Dispose ();
                Name = null;
            }
        }
    }
}