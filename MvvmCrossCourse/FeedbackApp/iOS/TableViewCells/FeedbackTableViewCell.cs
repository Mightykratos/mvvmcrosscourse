﻿using System;

using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using UIKit;
using MvvmCrossCourse.Core.ViewModels;

namespace MvvmCrossCourse.iOS.TableViewCells
{
    public partial class FeedbackTableViewCell : MvxTableViewCell
    {
        public static readonly NSString Key = new NSString("FeedbackTableViewCell");
        public static readonly UINib Nib;

        static FeedbackTableViewCell()
        {
            Nib = UINib.FromName("FeedbackTableViewCell", NSBundle.MainBundle);
        }

        protected FeedbackTableViewCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
            this.DelayBind(() =>
            {
                var set = this.CreateBindingSet<FeedbackTableViewCell, FeedbackItemViewModel>();
                set.Bind(Name).To(vm => vm.FullName);
                set.Bind(Feedback).To(vm => vm.Feedback);
                set.Apply();
            });
        }
    }
}
