﻿using System;
using MvvmCross.Platform.Converters;
using CoreGraphics;
using CoreAnimation;
namespace MvvmCrossCourse.iOS
{
    public class IntToTransformationValueConverter : MvxValueConverter<int,CGAffineTransform>
    {
        protected override CGAffineTransform Convert(int value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var rotation = (float)Math.PI * ((float)value / 360);
            return CGAffineTransform.MakeRotation(rotation * 2);
        }
    }
}
