﻿using System;
using MvvmCrossCourse.Core.Data;
using MvvmCrossCourse.Core.ViewModels;
using MvvmCross.Core.ViewModels;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace MvvmCrossCourse.Core.Services
{
    public class SimpsonsService : ISimpsonsService
    {
        public List<SimpsonViewModel> _simpsons { get; }

        public SimpsonsService()
        {
            _simpsons = new List<SimpsonViewModel> {
                new SimpsonViewModel {
                    Name = "Homer Simpson",
                    HeadshotUrl = "homer",
                    Email = "donutlover@aol.com",
                    Dob = new DateTime(1965, 1, 24),
                    Gender = Gender.Male,
                    Quote = "Oh boy, dinnertime. The perfect break between work and drunk!",
                    Pitch = 1.0f
                },

                new SimpsonViewModel {
                    Name = "Marge Simpson",
                    HeadshotUrl = "marge",
                    Email = "mamma_marge@gmail.com",
                    Dob = new DateTime(1966, 6, 4),
                    Gender = Gender.Female,
                    Quote = "Go out on a Tuesday? Who am I, Charlie Sheen?",
                    Pitch = 1.3f
                },

                new SimpsonViewModel {
                    Name = "Bart Simpson",
                    HeadshotUrl = "bart",
                    Email = "troublemaker@gmail.com",
                    Dob = new DateTime(2006, 02, 02),
                    Gender = Gender.Male,
                    Quote = "I didn’t think it was physically possible, but this both sucks and blows.",
                    Pitch = 1.5f
                },

                new SimpsonViewModel {
                    Name = "Lisa Simpson",
                    HeadshotUrl = "lisa",
                    Email = "cooljazz@gmail.com",
                    Dob = new DateTime(2008, 12, 02),
                    Gender = Gender.Female,
                    Quote = "I’d be mortified if someone ever made a lousy product with the Simpson name on it.",
                    Pitch = 1.9f
                },

                new SimpsonViewModel {
                    Name = "Maggie Simpson",
                    HeadshotUrl = "maggie",
                    Email = "babysimpson@gmail.com",
                    Dob = new DateTime(2009, 09, 15),
                    Gender = Gender.Female,
                    Quote = "...",
                    Pitch = 2.2f
                },

                new SimpsonViewModel {
                    Name = "Krusty the Clown",
                    HeadshotUrl = "krusty",
                    Email = "funnyguy@krustytheclown.com",
                    Dob = new DateTime(1972, 04, 12),
                    Gender = Gender.Male,
                    Quote = "Does anybody hear me complaining about the breasts?",
                    Pitch = 0.6f
                },

                new SimpsonViewModel {
                    Name = "Apu Nahasapeemapetilon",
                    HeadshotUrl = "apu",
                    Email = "storekeeper@quikeemart.com",
                    Dob = new DateTime(1977, 09, 03),
                    Gender = Gender.Male,
                    Quote = "For the next five minutes, I’m going to party like it’s on sale for $19.99.",
                    Pitch = 1.2f
                },

                new SimpsonViewModel {
                    Name = "Comic Book Guy",
                    HeadshotUrl = "comicbookguy",
                    Email = "comics@nerd.com",
                    Dob = new DateTime(1969, 10, 30),
                    Gender = Gender.Male,
                    Quote = "Oh, loneliness and cheeseburgers are a dangerous mix.",
                    Pitch = 0.8f
                },

                new SimpsonViewModel {
                    Name = "Milhouse van Houten",
                    HeadshotUrl = "milhouse",
                    Email = "iammilhouse@aol.com",
                    Dob = new DateTime(2006, 10, 12),
                    Gender = Gender.Male,
                    Quote = "Remember the time he ate my goldfish, and you lied to me and said I never had any goldfish. Then why’d I have the bowl, Bart? Why did I have the bowl",
                    Pitch = 1.6f
                },

                new SimpsonViewModel {
                    Name = "Moe Szyslak",
                    HeadshotUrl = "moeszyslak",
                    Email = "moe@moesbar.com",
                    Dob = new DateTime(1961, 09, 22),
                    Gender = Gender.Male,
                    Quote = "I’ve been called ugly, pug ugly, fugly, pug fugly, but never ugly ugly.",
                    Pitch = 0.8f
                },

                new SimpsonViewModel {
                    Name = "Mr. Burns",
                    HeadshotUrl = "mrburns",
                    Email = "theman@boss.com",
                    Dob = new DateTime(1950, 11, 7),
                    Gender = Gender.Male,
                    Quote = "This is a thousand monkeys working at a thousand typewriters. Soon they’ll have written the greatest novel known to man. Let’s see. It was the best of times, it was the “blurst” of times! You stupid monkey!",
                    Pitch = 0.5f
                },

                new SimpsonViewModel {
                    Name = "Sideshow Bob",
                    HeadshotUrl = "sideshowbob",
                    Email = "secondfiddle@krustytheclown.com",
                    Dob = new DateTime(1982, 4, 9),
                    Gender = Gender.Male,
                    Quote = "I did once try to kill the world's greatest lover. But then I realised there are laws against suicide",
                    Pitch = 0.6f
                },

                new SimpsonViewModel {
                    Name = "Waylon Smithers",
                    HeadshotUrl = "waylonsmithers",
                    Email = "lackey@boss.com",
                    Dob = new DateTime(1986, 5, 30),
                    Gender = Gender.Male,
                    Quote = "What happened to that mini-cell phone I gave you, sir?",
                    Pitch = 0.7f
                },
            };
        }

        public IList<SimpsonViewModel> GetSimpsons()
        {
            return _simpsons.OrderBy(x => x.Name).ToList();
        }
    }
}

