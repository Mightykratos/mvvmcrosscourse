﻿using AVFoundation;
using MvvmCrossCourse.Core.Services;

namespace MvvmCrossCourse.iOS.Services
{
    public class TextToSpeechService : ITextToSpeechService
    {
        public void Speak(string text, float pitch)
        {
            var speechSynthesizer = new AVSpeechSynthesizer();
            speechSynthesizer.SpeakUtterance(new AVSpeechUtterance(text)
            {
                Rate = AVSpeechUtterance.DefaultSpeechRate,
                Voice = AVSpeechSynthesisVoice.FromLanguage("en-US"),
                Volume = .5f,
                PitchMultiplier = pitch
            });
        }
    }
}

