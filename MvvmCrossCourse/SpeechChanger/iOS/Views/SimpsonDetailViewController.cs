﻿using System;

using UIKit;
using MvvmCross.iOS.Views.Presenters.Attributes;
using MvvmCross.iOS.Views;
using MvvmCrossCourse.Core.ViewModels;
using MvvmCross.Binding.BindingContext;
using MvvmCrossCourse.iOS.Converters;
using MvvmCross.Binding.iOS.Views.Gestures;

namespace MvvmCrossCourse.iOS.Views
{
    [MvxChildPresentationAttribute()]
    public partial class SimpsonDetailViewController : MvxViewController<SimpsonDetailViewModel>
    {
        public SimpsonDetailViewController() : base("SimpsonDetailViewController", null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            NavigationController.NavigationBar.Translucent = false;

            var set = this.CreateBindingSet<SimpsonDetailViewController, SimpsonDetailViewModel>();
            set.Bind(Image).To(vm => vm.SimpsonViewModel.HeadshotUrl).WithConversion(new ImageValueConverter());
            set.Bind(LblName).To(vm => vm.SimpsonViewModel.Name);
            set.Bind(LblEmail).To(vm => vm.SimpsonViewModel.Email);
            set.Bind(LblDob).To(vm => vm.SimpsonViewModel.Dob).WithConversion(new DateTimeValueConverter());
            set.Bind(LblGender).To(vm => vm.SimpsonViewModel.Gender);
            set.Bind(LblQuote).To(vm => vm.SimpsonViewModel.Quote);
            set.Bind(LblQuote.Tap()).For(x => x.Command).To(vm => vm.SpeakQuoteCommand);
            set.Apply();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            NavigationItem.Title = ViewModel.SimpsonViewModel.Name;
        }
    }
}

