using System.Threading.Tasks;
using MvvmCross.Core.ViewModels;

namespace MvvmCrossCourse.Core.ViewModels
{
    public class MainViewModel : MvxViewModel
    {
        public string Title => "MainViewModel";

        public MainViewModel()
        {
        }

        public override Task Initialize()
        {
            return base.Initialize();
        }

        public IMvxCommand ResetTextCommand => new MvxCommand(ResetText);

        private void ResetText()
        {
            Text = "Hello MvvmCross";
        }

        private string _text = "Hello MvvmCross";
        public string Text
        {
            get { return _text; }
            set { SetProperty(ref _text, value); }
        }
    }
}