﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using MvvmCrossCourse.Core.Api.Client;
using Plugin.Connectivity;
using Polly;

namespace MvvmCrossCourse.Core.Api.Services
{
    internal abstract class WebApiBaseService
    {
        protected readonly IWebApiClient WebApiClient;

        protected WebApiBaseService(IWebApiClient webApiClient)
        {
            if(webApiClient == null)
                throw new ArgumentNullException(nameof(webApiClient));

            WebApiClient = webApiClient;
        }

        protected async Task<TResult> ExecuteRemoteRequest<TResult>(Func<Task<TResult>> action)
        {
            TResult result = default(TResult);

            if (CrossConnectivity.Current.IsConnected)
            {
                try
                {
                    result = await Policy
                        .Handle<Exception>()
                        .WaitAndRetryAsync(
                            retryCount: 5,
                            sleepDurationProvider: retryAttamp => TimeSpan.FromSeconds(Math.Pow(2, retryAttamp))
                        )
                        .ExecuteAsync(action);
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                }
            }

            return result;
        }
    }
}
