﻿using System;
using System.Diagnostics;
using Akavache;
using Fusillade;
using MvvmCrossCourse.Core.Api.Client;
using MvvmCrossCourse.Core.Models;

namespace MvvmCrossCourse.Core.Api.Services
{
    public interface IAboutTheAppCmsService
    {
        IObservable<OverDeAppInfo> GetOverDeAppInfoAsync(Priority priority = Priority.UserInitiated);
    }

    class AboutTheAppCmsService : WebApiBaseService, IAboutTheAppCmsService
    {
        public AboutTheAppCmsService(IWebApiClient webApiClient) : base(webApiClient)
        {
        }

        public IObservable<OverDeAppInfo> GetOverDeAppInfoAsync(Priority priority)
        {
            try
            {
                var cache = BlobCache.LocalMachine;

                var cachedObject = cache.GetAndFetchLatest(
                    "overdeapp",
                    async () =>
                    {
                        try
                        {
                            return
                                await
                                    ExecuteRemoteRequest(
                                () => WebApiClient.GetAsync<OverDeAppInfo>(priority, "about"));
                        } catch (Exception e)
                        {
                            Debug.WriteLine(e.Message);
                            return null;
                        }
                    },
                    offset =>
                    {
                        TimeSpan elapsed = DateTimeOffset.Now - offset;
                        return elapsed > new TimeSpan(hours: 0, minutes: 1, seconds: 0);
                    });

                return cachedObject;
            }
            catch (Exception e){
                Debug.WriteLine(e.Message);
            }

            return null;
        }
    }
}

