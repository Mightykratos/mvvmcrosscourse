﻿using System;
using MvvmCross.Platform;
using MvvmCrossCourse.Core.Api.Services;

namespace MvvmCrossCourse.Core.Api.Client
{
    public interface IApiClient
    {
        IAboutTheAppCmsService AboutTheAppCmsService { get; }
    }

    public class ApiClient : IApiClient
    {
        private readonly Lazy<IAboutTheAppCmsService> _aboutTheAppCmsService = new Lazy<IAboutTheAppCmsService>(Mvx.Resolve<IAboutTheAppCmsService>);

        public IAboutTheAppCmsService AboutTheAppCmsService => _aboutTheAppCmsService.Value;
    }
}
