﻿using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace MvvmCrossCourse.Core.Api.Client.Resolvers
{

    public interface IHttpResponseResolver
    {
        Task<TResult> ResolveHttpResponseAsync<TResult>(HttpResponseMessage responseMessage);
    }

    public class SimpleJsonResponseResolver
        : IHttpResponseResolver
    {
        public async Task<TResult> ResolveHttpResponseAsync<TResult>(HttpResponseMessage responseMessage)
        {
            if (!responseMessage.IsSuccessStatusCode)
            {
                return default(TResult);
            }

            var responseAsString = await responseMessage.Content.ReadAsStringAsync();

            const string format = "d-M-yyyy HH:mm";
            var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };


            var toReturn = JsonConvert.DeserializeObject<TResult>(responseAsString, dateTimeConverter);
            return toReturn;
        }
    }
}
