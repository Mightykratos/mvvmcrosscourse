using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using MvvmCross.Platform.IoC;
using MvvmCrossCourse.Core.Api.Client;

namespace MvvmCrossCourse.Core
{
    public class App : MvvmCross.Core.ViewModels.MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
               .EndingWith("Service")
               .AsInterfaces()
               .RegisterAsLazySingleton();

            CreatableTypes()
                .EndingWith("Client")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            CreatableTypes()
                .EndingWith("Repository")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            Mvx.RegisterSingleton<IWebApiClient>(new WebApiClient(Urls.BaseUrl));
            Mvx.RegisterSingleton<IApiClient>(new ApiClient());
            Mvx.ConstructAndRegisterSingleton<IMvxNavigationCache, MvxNavigationCache>();
            Mvx.ConstructAndRegisterSingleton<IMvxNavigationService, MvxNavigationService>();
            Mvx.RegisterType<IMvxAppStart, AppStart>();
        }
    }
}
